import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.control.TextField;


public class WeatherAppGUI extends Application {

    Button goButton;
    TextField searchBar;
    Label zipCodeLabel;
    Label currentWeatherLabel;
    Label errorMessage;
    Label location;
    Label currentTempLabel;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane layout = new Pane();

        primaryStage.setTitle("Simple Weather Application");


        goButton = new Button(" Go ");
        goButton.setTranslateX(275);
        goButton.setTranslateY(10);
        goButton.setDefaultButton(true);
        goButton.setOnAction(event -> {
                    setCurrentWeather(searchBar.getText());
                }
        );

        searchBar = new TextField();
        searchBar.setPromptText("Ex. 95682");
        searchBar.setTranslateX(100);
        searchBar.setTranslateY(10);

        errorMessage = new Label();
        errorMessage.setTranslateX(100);
        errorMessage.setTranslateY(35);


        zipCodeLabel = new Label("Zip Code:");
        zipCodeLabel.setTranslateX(15);
        zipCodeLabel.setTranslateY(15);

        location = new Label();
        location.setTranslateX(15);
        location.setTranslateY(60);

        currentWeatherLabel = new Label();
        currentWeatherLabel.setText("Current Weather: ");
        currentWeatherLabel.setTranslateX(15);
        currentWeatherLabel.setTranslateY(120);

        currentTempLabel = new Label();
        currentTempLabel.setText("Current Temperature: ");
        currentTempLabel.setTranslateX(15);
        currentTempLabel.setTranslateY(90);

        layout.getChildren().addAll(goButton, searchBar, zipCodeLabel, currentWeatherLabel,
                errorMessage, location, currentTempLabel);

        Scene scene = new Scene(layout, 400, 200);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

    }


    public void setCurrentWeather(String userInput) {
        Platform.runLater(() -> {
            if (!userInput.isEmpty()) {
                WeatherApp weatherApp = new WeatherApp(userInput);
                if (!weatherApp.Zipcode().equals("invalid")) {
                    location.setText(weatherApp.Zipcode());
                    currentWeatherLabel.setText("Current Weather: " + weatherApp.getWeather());
                    currentTempLabel.setText("Current Temperature: " + weatherApp.getTemperature());
                    errorMessage.setText(null);
                } else {
                    errorMessage.setText("* Enter a valid Zip Code!");
                    errorMessage.setTextFill(Color.RED);
                    searchBar.setPromptText("Ex. 95682");
                    searchBar.setText(null);
                }
            }
            else
            {
                errorMessage.setText("* Enter a valid Zip Code!");
                errorMessage.setTextFill(Color.RED);
                searchBar.setPromptText("Ex. 95682");
                searchBar.setText(null);

            }
        });
    }
}
