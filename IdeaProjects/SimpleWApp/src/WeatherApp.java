import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

public class WeatherApp {
    private final String ACCESS_TOKEN = "a4e52c2993894d3a";
    private JsonElement json;
    private String apiURL;

    public WeatherApp(String zipCode)
    {

        try
        {
            // Encode the user-supplied data to neutralize any special chars

            // Construct API URL
            apiURL = "https://api.wunderground.com/api/" +
                    ACCESS_TOKEN + "/conditions/forecast10day/q/" + zipCode + ".json";

            // Create URL object
            URL weatherUndergroundURL = new URL(apiURL);

            // Create InputStream Object
            InputStream is = weatherUndergroundURL.openStream();

            // Create InputStreamReader
            InputStreamReader isr = new InputStreamReader(is);

            // Parse input stream into a JsonElement
            JsonParser parser = new JsonParser();
            json = parser.parse(isr);
        }
        catch (java.net.MalformedURLException mue)
        {
            System.out.println("Malformed URL");
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("IO Error");
        }
    }

    public String Zipcode()
    {
        try{
            return json.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
        }
        catch (java.lang.NullPointerException npe)
        {
            return "invalid";
        }

    }

    public String getWeather() // returns current weather conditions
    {
        return json.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
    }


    public double getTemperature() //return current temperature
    {
        return json.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsDouble();
    }

    public static void main(String[] args)
    {
        WeatherApp weatherApp = new WeatherApp("95682");
        Double currentTemp = weatherApp.getTemperature();
        String currentWeather = weatherApp.getWeather();
        String cityState = weatherApp.Zipcode();
        System.out.println(currentTemp);
    }
}
